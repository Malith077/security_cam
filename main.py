import cv2

cap = cv2.VideoCapture(0)

if not(cap.isOpened()):
	print('Couldnt open cam')

object_classifier = cv2.CascadeClassifier('models/upperbody_recognition_model.xml')

cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
i = 0

while(True):
	ret, frame = cap.read()
	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	objects = object_classifier.detectMultiScale(
		gray,
		scaleFactor=1.1,
		minNeighbors=5,
		minSize=(30, 30),
		flags=cv2.CASCADE_SCALE_IMAGE
		)

	for (x, y, w, h) in objects:
		cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

	print(len(objects))
	
	
	if len(objects)>0:
		i=i+1
		cv2.imwrite(filename='file' + str(i) + '.jpg', img=frame)
	
	cv2.imshow('preview', frame)
	
	if cv2.waitKey(1) & 0xFF == ord('q'):
		break;

cap.release()
cv2.destroyAllWindows()

